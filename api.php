<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require 'db.php';
$helper = new DB();

//read JSON input
$input = json_decode(file_get_contents('php://input')) or die(json_encode(array("error"=>"NO_DATA")));

//array parseing to extract message $load
$load = (array) $input->load;

//prepare targeted email in a single 'quoted' CSV format
$target = "'".implode("','", $input->target)."'";

//select target users' by email
$user = $helper->getUsersByEmails($target);


/**
 * array: ids
 * to split multiple platforms
 *
 * Structure sample:
 *  Array(
 *      gcm => Array(
 *      ...
 *      ),
 *
 *      apn => Array(
 *      ...
 *      ),
 *          
 *  )
 */
$ids = array();
foreach ($user as $key => $value) {
    $ids[$value['platform']][] = $value['service_id'];
}


if (empty($ids))
    die(json_encode(array("error"=>"NO_USERS_FOUND")));
$nid = $helper->AddNotification($load);

echo $helper->sendNotification($ids['gcm'], $load, $nid);
?>